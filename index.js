
/*
JSON Objects
    - JSON stands for JavaScript object Notation
    - JSON can also be use in other programming languages
    - Do not confuse Javascript objects with JSON
    - JSON is used for serializing defferent data types into bytes
        -serialization - process of converting data types into series of bytes for easier transmission or transfer of information


        SYNTAX:
            {
                "propertyA" : "valueA"
                "propertyB" : "valueB"
            }
        
        SAMPLE:
        let person = {
            "name" : "Yoon",
            "birthDay" : "March 9, 1993"
        };
*/

let person = {
    "name" : "Yoon",
    "birthDay" : "March 9, 1993"
};

/*
//JSON Objects
{
    "city" : "Quezon city",
    "provice" : "Metro Manila",
    "country" : "Philippines"
}

//JSON ARRAYS
"cities" : [
    {
        "city" : "Quezon City",
        "province" : "Metro Manila",
        "country" : "Philippines"
    },
    {
        "city" : "Cebu City",
        "province" : "Cebu",
        "country" : "Philippines"
    }
]
*/

//JSON METHODS
    //JSON objects contain methods for parsing and converting data into stringified JSON

let batchesArr = [
    {batchName: "Batch 169"},
    {batchName: "Batch 170"},
]

console.log(JSON.stringify(batchesArr))


let data = JSON.stringify({
    name: "Luke",
    age: 45,
    address: {
        city: "Manila",
        country: "Philippines"
    }
})
console.log(data)

//Using Stringify method with variables

//User Details
let firstName = prompt("First Name:");
let lastName = prompt("Last Name:");
let age = prompt("Age:");
let address = {
    city: prompt("City:"),
    Country: prompt("Counrtry:")
};

let data2 = JSON.stringify({
    firstName : firstName,
    lastName : lastName,
    age : age,
    address : address
});
console.log(data2);

//Converting 

letbatchesJSON = `[
    {
        "bathName" : "Batch 169"
    },
    {
        "bathName" : "Batch 169"
    }
]`
console.log(JSON.parse(letbatchesJSON));
console.log(JSON.parse(data2));